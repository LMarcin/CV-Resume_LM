// ===== Scroll to Top ====
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 900);
});

// ===== Typed ====

$(function(){
  $(".type").typed({
    strings: [ "Imaginative", "Creative", "Perfectionnist", "Passionnate", "<span></span>",],
    typeSpeed: 10,
    backDelay:1000,
    loop: true,
    loopCount: false,
    showCursor: false,
  });
});

$(function(){
  $(".hello").typed({
    strings: [ "Hi, I'm Loïs Marcin",],
    typeSpeed: 0.1,
    backDelay:1000,
    loop: false,
    loopCount: false,
    showCursor: false,
  });
});
